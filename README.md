# yii2-module/yii2-module-all-suite

Metapackage that lists all the packages under the yii2-module namespace.

![coverage](https://gitlab.com/yii2-module/yii2-module-all-suite/badges/master/pipeline.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install yii2-module/yii2-module-all-suite ^8`


## Included packages

| Package Pipeline | Package Coverage | Package Name |
|:-----------------|:-----------------|:-------------|
| ![coverage](https://gitlab.com/yii2-module/yii2-module-helper/badges/main/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-module/yii2-module-helper/badges/main/coverage.svg?style=flat-square) | [yii2-module/yii2-module-helper](https://gitlab.com/yii2-module/yii2-module-helper) |
| ![coverage](https://gitlab.com/yii2-module/yii2-crud/badges/main/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-module/yii2-crud/badges/main/coverage.svg?style=flat-square) | [yii2-module/yii2-crud](https://gitlab.com/yii2-module/yii2-crud) |
| ![coverage](https://gitlab.com/yii2-module/yii2-currency/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-module/yii2-currency/badges/master/coverage.svg?style=flat-square) | [yii2-module/yii2-currency](https://gitlab.com/yii2-module/yii2-currency) |
| ![coverage](https://gitlab.com/yii2-module/yii2-dgfip-ensap/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-module/yii2-dgfip-ensap/badges/master/coverage.svg?style=flat-square) | [yii2-module/yii2-dgfip-ensap](https://gitlab.com/yii2-module/yii2-dgfip-ensap) |
| ![coverage](https://gitlab.com/yii2-module/yii2-export/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-module/yii2-export/badges/master/coverage.svg?style=flat-square) | [yii2-module/yii2-export](https://gitlab.com/yii2-module/yii2-export) |
| ![coverage](https://gitlab.com/yii2-module/yii2-information/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-module/yii2-information/badges/master/coverage.svg?style=flat-square) | [yii2-module/yii2-information](https://gitlab.com/yii2-module/yii2-information) |
| ![coverage](https://gitlab.com/yii2-module/yii2-insee-ban/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-module/yii2-insee-ban/badges/master/coverage.svg?style=flat-square) | [yii2-module/yii2-insee-ban](https://gitlab.com/yii2-module/yii2-insee-ban) |
| ![coverage](https://gitlab.com/yii2-module/yii2-insee-catjur/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-module/yii2-insee-catjur/badges/master/coverage.svg?style=flat-square) | [yii2-module/yii2-insee-catjur](https://gitlab.com/yii2-module/yii2-insee-catjur) |
| ![coverage](https://gitlab.com/yii2-module/yii2-insee-cog/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-module/yii2-insee-cog/badges/master/coverage.svg?style=flat-square) | [yii2-module/yii2-insee-cog](https://gitlab.com/yii2-module/yii2-insee-cog)
| ![coverage](https://gitlab.com/yii2-module/yii2-insee-naf/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-module/yii2-insee-naf/badges/master/coverage.svg?style=flat-square) | [yii2-module/yii2-insee-naf](https://gitlab.com/yii2-module/yii2-insee-naf) |
| ![coverage](https://gitlab.com/yii2-module/yii2-insee-sirene/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-module/yii2-insee-sirene/badges/master/coverage.svg?style=flat-square) | [yii2-module/yii2-insee-sirene](https://gitlab.com/yii2-module/yii2-insee-sirene) |
| ![coverage](https://gitlab.com/yii2-module/yii2-log/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-module/yii2-log/badges/master/coverage.svg?style=flat-square) | [yii2-module/yii2-log](https://gitlab.com/yii2-module/yii2-log) |
| ![coverage](https://gitlab.com/yii2-module/yii2-merge/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-module/yii2-merge/badges/master/coverage.svg?style=flat-square) | [yii2-module/yii2-merge](https://gitlab.com/yii2-module/yii2-merge) |
| ![coverage](https://gitlab.com/yii2-module/yii2-user/badges/main/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-module/yii2-user/badges/main/coverage.svg?style=flat-square) | [yii2-module/yii2-user](https://gitlab.com/yii2-module/yii2-user) |


## License

MIT (See [license file](LICENSE)).
